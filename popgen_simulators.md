List of simulators in population genetics
=========================================

# Summary Table


Software           | last version | Type             | Selection ? | Gene conversion ? | Haploid  ? | demography | Output tree
-------------------|--------------|------------------|-------------|-------------------|------------|------------|------------
ms                 | 2017         | Backward  (WF)   | No          | Yes               |            | Yes        | Yes
msms               | 2014         | Backward  (WF)   | Yes         | No                | No?        | Yes        | Yes
msHOT              | 2008         | Backward  (WF)   | No          | Yes               | Yes        | Yes        | Yes
msPrime            | 2016         | Backward  (WF)   | No          | No                | No         | Yes        | Yes
msPro              | NA           | Backward  (WF)   | No          | Yes               | Yes        | Yes        | Yes
SLIM 2             | 2017         | Forward  (WF)    | Yes         | Yes               | Yes        | Yes        | Yes
FastSimCoal        | 2017         | Backward  (WF)   | No          | No                | Yes        | Yes        | Yes
Cosi2              | 2016         | Backward  (WF)   | Yes         | Yes               | Unclear    | Yes        | No
scrm               | 2016         | Backward  (WF)   | No          | No                | No         | Yes        | ND
SimBac             | 2016         | Backward  (WF)   | No          | Yes               | Yes        | No         | No
SelSim             | 2016         | Backward  (WF)   | No          | Yes               | Yes        | No         | No
$\delta a\delta i$ | 2017         | Diffusion        | -           | -                 | -          | -          | -
GO Fish            | 2017         | Forward  (WF)    | Yes         | No?               | Yes        | Yes        | Yes
PReFerSim          | 2016         | Forward (PRF)    | Yes         | No                | Yes        | Yes        | ?
fwdpp              | 2017         | Forward (WF)     | Yes         | Yes               | Yes        | Yes        | Yes
ftprime_ms         | 2018         | Forward          | Yes         | Yes               | Yes        | Yes        | Yes
ARGON              | 2016         | Discrete Time WF | No          | Yes               | Yes        | Yes        | Yes
simuPOP            | 2017         | Foward           | Yes         | Yes               | Yes        | Yes        | Yes


[ms](http://home.uchicago.edu/~rhudson1/source/mksamples.html)
--
Main soft to generate samples under neutral models.

**Hudson**, *Generating samples under a Wright-Fisher neutral model of genetic variation*, **Bioinformatics**, 2002

Can simulate population samples under variety of assumptions:

- migration
- recombination rate
- population size

The samples are generated with the the coalescent approaches, using the infinite site model of mutation and the small sample approximation of coalescent.

Can model recombination (crossing over) with a rate $\rho = 2 \times 2N_0\times r$ where $r$ is the probability of cross-over per generation between the ends of the locus being simulated.

Can model gene-conversion. This process is assumed to initiate between a specified pair of sites in a given chromosome with probability $g$. $f = g/r$. The recombined track has a mean length of $\lambda$.

# [msms](http://www.mabs.at/ewing/msms/about.shtml)

[**Gregory Ewing and Joachim Hermisson** *MSMS: A Coalescent Simulation Program Including Recombination, Demographic Structure, and Selection at a Single Locus*, **Bioinformatics**, 2010](http://doi.org/10.1093/bioinformatics/btq322)

> MSMS is a coalescent simulator that models itself off Hudsons ms in usage and includes selection. It is fast, often faster than ms, and portable running on Mac OSX, windows and Linux. By using this tool, one can study the patterns of selection in complicated demographic scenarios.

On the diploidy: in the paper, its written: "Individuals can be haploid or diploid"

gene conversion not implemented.

# msHOT

[**Hellenthal, Stephens**, *msHOT: modifying Hudson's ms simulator to incorporate crossover and gene conversion hotspots*, **Bioinformatics**, 2007](https://academic.oup.com/bioinformatics/article/23/4/520/180722)

> We have incorporated both crossover and gene conversion hotspots into an existing coalescent-based program for simulating genetic variation data for a sample of chromosomes from a population.

No Selection.


# [msPrime](https://msprime.readthedocs.io/en/stable/introduction.html)
[**Kelleher, Etheridge, McVean**, *Efficient Coalescent Simulation and Genealogical Analysis for Large sample sizes*, **PLoS Computational Biology**, 2016](http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1004842)

> In this paper we consider a diploid model in which we have a sequence of m discrete sites that are indexed from zero

Use HDF5 format to store tree: much more efficient than Newick.

Gene conversion planned, but [not likely to be implemented anytime soon](https://github.com/jeromekelleher/msprime/issues/11#issuecomment-322431072)

No selection

# [msPro](http://www.sendou.soken.ac.jp/esb/innan/InnanLab/Index_En.html)

Code not online yet.

[**Akita, Takuno, and Innan**, _Coalescent Framework for Prokaryotes Undergoing Interspecific Homologous Recombination._ **Heridity**, 2018](http://www.nature.com/articles/s41437-017-0034-1)

> We present a simulation framework for generating SNP (single-nucleotide polymorphism) patterns that allows external DNA integration into host genome from other species

> "Prokaryotes are haploid and do not undergo meiosis; therefore, recombination mechanisms in prokaryotes are considerably different than those in eukaryotes. Nevertheless, the coalescent framework can be applied to prokaryotes with a relatively simple modification, that is, because circular chromosome in prokaryotes requires double "crossing-over" to exchange a DNA fragment, recombination can be considered as an event analogous to meiotic gene conversion

--> No Selection

# [SLIM 2](https://messerlab.org/slim/)

[**Haller and Messer**, _SLiM 2: Flexible, Interactive Forward Genetic Simulations_, **MBE**, 2017](https://academic.oup.com/mbe/article/34/1/230/2670194)

--> Slower, but might be possible if $N_e$ is not too big.

![slim 2 benchmark](media/sim-2-benchmark.png)

# [FastSimCoal2](http://cmpg.unibe.ch/software/fastsimcoal2/)

[**Excoffier, Dupanloup, Huerta-Sancher, Sousa, Foll**, *Robust demographic inference from genomic and SNP data*, **PLoS Genetics**, 2013](http://journals.plos.org/plosgenetics/article?id=10.1371/journal.pgen.1003905)

> fastsimcoal can handle very complex evolutionary scenarios including an arbitrary migration matrix between samples, historical events allowing for population resize, population fusion and fission, admixture events, changes in migration matrix, or changes in population growth rates. The time of sampling can be specified independently for each sample, allowing for serial sampling in the same or in different populations.

>  fastsimcoal can simulate data in genomic regions with arbitrary recombination rates, thus allowing for recombination hotspots of different intensities at any position. fastsimcoal implements a new approximation to the ancestral recombination graph in the form of sequential Markov coalescent allowing it to very quickly generate genetic diversity for >100 Mb genomic segments.

No selection nor gene conversion


# [Cosi2](https://github.com/broadinstitute/cosi2)

[**Shlyakhter, Sabeti, Schaffner**, *Cosi2: an efficient simulator of exact and approximate coalescent with selection*, **Bioinformatics**, 2014](https://academic.oup.com/bioinformatics/article/30/23/3427/207410)

> cosi2 is an efficient coalescent simulator with support for selection, population structure, variable recombination rates, and gene conversion. It supports exact and approximate simulation modes.

On selection:

> The use of the original coalescent allows cosi2 to support simulation of **positive** selection using the standard structured coalescent approach.

> Selection is implemented using the structured coalescent approach (Teshima and Innan, 2009). At start of simulation, sampled chromosomes are partitioned into two classes based on their allelic state at the selection site. Coalescence happens only within the same allelic class, with coalescence rate based on the frequency trajectory of the causal allele. The frequency trajectory of the selected allele can be deterministic (based on causal allele age and present-day frequency), or can be provided externally; the latter mode permits the use of stochastic trajectories generated for any selection model.

Positive selection is possible. No negative selection is possible.
The documentation on selection is as follow:

_Specifying the selected sweep:_

    pop_event sweep <label> \
    <pop> \
    <Tend> \
    <selection coefficient> \
    <position of causal allele (0.0-1.0)> \
    <freq at Tend> \

_\<pop> gives the population in which the advantageous allele is born. \<Tend> gives the generation at which sweep ends (in the forward-time sense). The position of the advantageous allele is specified as a floating-point number in the range 0.0-1.0, giving its relative position within the simulated region (for example, 0.5 puts the advantageous allele in the middle of the region). The frequency of the advantageous allele at time \<Tend> is specified as a number in the range 0.0-1.0 (not as a chromosome count)._


> Cosi2 supports population structure, population size changes, bottlenecks and migrations. Recombination rate can be varied along the simulated region, and the program includes a utility to generate genetic maps with recombination hotspots. Cosi2 also supports a simple model of gene conversion.

How they handle haploidy isn't clear.

# [scrm](https://scrm.github.io/)

[**Staab, Zhu, Metzler, Lunter**, *scrm: efficiently simulating long sequences using approximated coalescent with Recombination*, **Bioinformatics**, 2015](https://academic.oup.com/bioinformatics/article/31/10/1680/176764)

> Coalescent-based simulation software for genomic sequences allows the efficient in silico generation of short- and medium-sized genetic sequences. However, the simulation of genome-size datasets as produced by next-generation sequencing is currently only possible using fairly crude approximations.

> scrm can **not** simulate
>
> - gene conversions (-c in ms) and
> - a fix number of segregating sites (-s),


# [SimBac](http://mgen.microbiologyresearch.org/content/journal/mgen/10.1099/mgen.0.000044#tab2)

> We present SimBac, a whole-genome bacterial evolution simulator that is roughly two orders of magnitude faster than previous software and includes a more general model of bacterial evolution, allowing both within and between-species homologous recombination. Since methods modelling bacterial recombination generally focus on only one of these two modes of recombination, the possibility to simulate both allows for a general and fair benchmarking

> Although SimBac generalizes the applicability of Sim MLST, it currently lacks the wide set of options of some simulators of evolution, in particular of forward simulators that allow very general demographic, speciation, selection, migration and rate variation patterns (e.g. Chadeau-Hyam et al., 2008; Dalquen et al., 2012). **In fact, many of these features present considerable methodological hurdles in being incorporated in computationally efficient coalescent simulators**

# [$\delta a\delta i$](https://bitbucket.org/gutenkunstlab/dadi/)

[**Gutenkunst, Hernandez, Williamson, Bustamante, McVean**, *Inferring the Join Demographic history of multiple populations from Multidimensional SNP Frequency data*, **PLoS Genetics**, 2009](http://dx.plos.org/10.1371/journal.pgen.1000695)

>∂a∂i implements methods for demographic history and selection inference from genetic data, based on diffusion approximations to the allele frequency spectrum. One of ∂a∂i's main benefits is speed: fitting a two-population model typically takes around 10 minutes, and run time is independent of the number of SNPs in your data set. ∂a∂i is also flexible, handling up to three simultaneous populations, with arbitrary timecourses for population size and migration, plus the possibility of admixture and population-specific selection.

Not a simulator. It can generate the SFS given a demographic scenario. Doest not simulate genetic data.
Approximation of the forward single-locus WF algo using diffusion equation.

# [GO Fish](http://dl42.github.io/ParallelPopGen/)

[**Lawrie**, *Acceleration Wright Fisher Forward Simulations on the Graphic Processing Unit*, **G3**, 2017](https://doi.org/10.1534/g3.117.300103)

> GO Fish can simulate mutations across multiple populations for comparative population genomics, with no limits to the number of populations allowed. Population size, migration rates, inbreeding, dominance, and mutation rate are all user-specifiable functions capable of varying over time and between different populations. Selection is likewise a user-specifiable function parameterized not only by generation and population, but also by allele frequency, allowing for the modeling of frequency-dependent selection as well as time-dependent and population specific selection.

Haploid possible by setting the indreeding coefficient:
> (F = 0) := diploid, outbred; (F = 1) := haploid, inbred

No recombination or gene conversion.

# [PReFerSim](https://github.com/LohmuellerLab/PReFerSim)

[**Ortega-Del Vecchyo, Marsden, Lohmueller**, *PReFerSim: fast simuation of demography and selection under the poisson random field model*, **Bioinformatics**, 2016](https://academic.oup.com/bioinformatics/article-abstract/32/22/3516/2525599)

> Here, we present a new program, PReFerSim, which combines these approaches and performs forward simulations under the PRF model for a single population. Two features distinguish PReFerSim from existing implementations of the PRF model and forward simulation programs. First, PReFerSim can track allele frequencies over time, and the ages and trajectories of individual mutations. Second, PReFerSim can simulate large amounts of genetic variation data with __free recombination__ quickly.

free-mutation -> mutation rate = 1.
In PRF, no linkage.

> PReFerSim will simulate genetic variation data under the Poisson Random Field model. Each generation a Poisson number (with meanh/2, whereh is the population-scaled mutation rate) of mutations enter the population.

> PReFerSim allows users to consider a variety of models of selection and demography.


# [SFS_Code](http://sfscode.sourceforge.net/SFS_CODE/index/index.html)

Foward Simulation, too slow.

# [fwdpp](http://molpopgen.github.io/fwdpp/)

> Rather than attempt to provide a general program capable of simulating a wide array of models under standard modeling assumptions akin to ms, SLiM, or sfs_code, fwdpp instead abstracts the fundamental operations required for implementing a forward simulation under custom models

> The intent of the library is to provide generic routines for mutation, recombination, migration, and sampling of gametes proportionally to their fitnesses in a finite population of N diploids

Python Wrapper: [https://github.com/molpopgen/fwdpy11](https://github.com/molpopgen/fwdpy11)

# [ftprime](https://github.com/ashander/ftprime)

[**Kelleher, Thornton, Ashander, Ralph**, *Efficient pedigree recording for fast population genetics simulation*, **BioRxiv**, 2018](http://dx.doi.org/10.1101/248500)

They use pedigree recording with a msprime Table API to store efficiently the genotype and topological informations. This allow them to not consider neutral mutations, which in turn inscreases the speed of the simulation. The relation between `fwdpp`, `simuPOP` and `ftprime` isn't clear to me yet. It seems that they just implemented their method using the `fwdpp` or `simuPOP` libraries, and by adding a part on the msprime table api. The paper is still being edited since the publication on BioRxiv, so it's not sure whether it will be a good tool for us. Besides, it might be still too slow.

> "In this paper, we describe a storage method for succinct tree sequences (and hence, genome sequence) as well as an algorithm for simplifying these. The data structure is succinct in the sense that its' space usage is close to optimal, while still allowing efficient retrieval of information"

> one could use the methods described here to generate succinct tree sequences under coalescent processes not currently implemented in msprime, such as the coalescent with gene conversion [Wiuf and Hein, 2000], using the structured coalescent to model various forms of natural selection [Kaplan et al., 1988, 1989, Braverman et al., 1995], or the coalescent within a known pedigree. For such models, one could in principle generate tables of nodes and edges to be simplied in msprime. The resulting succinct tree sequence object would be in the same format as those generated by msprime's built-in simulate function, and therefore compatible with existing methods for downstream analyses

On ftprime:

> The purpose of this package is to provide python code to easily store ancestry information from a forwards-time, individual-based simulation, using msprime's "tree sequence" data format, so that after the simulation, we can
>
> - have the entire tree sequence (equivalent to the ARG) of the final generation:w
> - put down neutral mutations on the tree sequence afterwards without carrying them along in the simulation, and
> - use msprime to efficiently store results and quickly compute statistics.


# [ARGON](https://github.com/pierpal/ARGON)

[**Palamara**, _ARGON: fast, whole-genome simulation of the discrete time Wright-fisher process_, **Bioinformatics**, 2016](https://academic.oup.com/bioinformatics/article/32/19/3032/2196513)

> We present a simulator (ARGON) for the DTWF process that scales up to hundreds of thousands of samples and whole-chromosome lengths, with a time/memory performance comparable or superior to currently available methods for coalescent simulation. The simulator supports arbitrary demographic history, migration, Newick tree output, variable mutation/recombination rates and gene conversion, and efficiently outputs pairwise identical-by-descent (IBD) sharing data.

No selection

# [simuPOP](http://simupop.sourceforge.net/Main/HomePage)

[**Bo Peng and Marek Kimmal** _simuPOP: a forward-time population genetics simulation environment._ **Bioinformatics**, 2005](https://academic.oup.com/bioinformatics/article/21/18/3686/202296)

Need to check the speed, but given the paper on ftprime, it might be too slow.
