# Population genetics simulators

**NB**: I found a website referencing many pop gen softwares: [https://popmodels.cancercontrol.cancer.gov/gsr/](https://popmodels.cancercontrol.cancer.gov/gsr/)

You can filter simulators with what you'd like in terms of simulation features.

---

This repository contains a list of population genetics simulators that are more or less still under development.

Given the numerous simulators it exist in the wild, this list is probably not exhaustive.

This list was done with the idea of finding an efficient and rather complete simulator for bacterial populations.
We are looking for a simulator allowing:

- Haploid populations
- Selection
- Bacterial recombination, _i.e._ corresponding to gene conversion
- demographic scenario
- and efficient in runtime

The file [popgen_simulators.md](popgen_simulators.md) contains a summary table and for each software a small description or citation from the original paper or documentation.

The titles link to the webpage of the software if available, and the references to the original paper link to the publisher's page.

# Contributing

This repository is public, hence everyone can clone it and propose modification or addition.
